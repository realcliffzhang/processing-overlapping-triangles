from random import randint
from math import sqrt

def setup():
    global color_groups, time
    background(255)
    size(700, 700)
    color_groups = {}
    time = millis()

def draw():
    global time
    if millis() > time + 200:
        time = millis()
        x1, y1, x2, y2, x3, y3 = get_valid_rand_triangle()
        rand_color = (randint(0, 255), randint(0, 255), randint(0, 255))
        while rand_color in color_groups.keys():
            rand_color = (randint(0, 255), randint(0, 255), randint(0, 255))
        overlapped_colors = []
        for color_group in color_groups.items():
            cur_color = color_group[0]
            cur_tris = color_group[1]
            cur_overlap = False
            for cur_tri in cur_tris:
                overlapped = check_overlap((x1, y1, x2, y2, x3, y3), cur_tri)
                if overlapped:
                    cur_overlap = True
            if cur_overlap:
                overlapped_colors.append(cur_color)
        if len(overlapped_colors) == 0:
            print("no overlap")
            fill(*rand_color)
            triangle(x1, y1, x2, y2, x3, y3)
            color_groups[rand_color] = [(x1, y1, x2, y2, x3, y3)]
        elif len(overlapped_colors) == 1:
            print("one overlap")
            fill(*overlapped_colors[0])
            triangle(x1, y1, x2, y2, x3, y3)
            color_groups[overlapped_colors[0]].append((x1, y1, x2, y2, x3, y3))
        else:
            print("two or more overlap")
            prev_tris = []
            for overlapped_color in overlapped_colors:
                for prev_tri in color_groups[overlapped_color]:
                    prev_tris.append(prev_tri)
            for overlapped_color in overlapped_colors:
                del color_groups[overlapped_color]
            color_groups[rand_color] = []
            for prev_tri in prev_tris:
                triangle(*prev_tri)
                color_groups[rand_color].append(prev_tri)
            triangle(x1, y1, x2, y2, x3, y3)
            color_groups[rand_color].append((x1, y1, x2, y2, x3, y3))
        clear()
        background(255)
        for color_key in color_groups.keys():
            fill(*color_key)
            for tri in color_groups[color_key]:
                triangle(*tri)

def check_overlap(tri_1, tri_2):
    x1_1, y1_1, x2_1, y2_1, x3_1, y3_1 = tri_1
    x1_2, y1_2, x2_2, y2_2, x3_2, y3_2 = tri_2
    lines_intersect = False
    for line_i in (((x1_1, y1_1), (x2_1, y2_1)), ((x1_1, y1_1), (x3_1, y3_1)), ((x2_1, y2_1), (x3_1, y3_1))):
        for line_j in (((x1_2, y1_2), (x2_2, y2_2)), ((x1_2, y1_2), (x3_2, y3_2)), ((x2_2, y2_2), (x3_2, y3_2))):
            if check_lines_intersect(line_i, line_j):
                lines_intersect = True
    if lines_intersect:
        return True
    else:
        larger_tri = tri_1
        smaller_tri = tri_2
        if shoelace(*tri_2) > shoelace(*tri_1):
            larger_tri, smaller_tri = smaller_tri, larger_tri
        x1_1, y1_1, x2_1, y2_1, x3_1, y3_1 = smaller_tri
        x1_2, y1_2, x2_2, y2_2, x3_2, y3_2 = larger_tri
        sub_tris_areas = []
        for pts_i in (((x1_2, y1_2), (x2_2, y2_2)), ((x1_2, y1_2), (x3_2, y3_2)), ((x2_2, y2_2), (x3_2, y3_2))):
            sub_tri_area = shoelace(pts_i[0][0], pts_i[0][1], pts_i[1][0], pts_i[1][1], x1_1, y1_1)
            sub_tris_areas.append(sub_tri_area)
        if round(sum(sub_tris_areas), 1) == round(shoelace(*larger_tri), 1):
            return True
    return False

def check_lines_intersect(line_1, line_2):
    slope_1, y_int_1 = slope_intercept(*line_1)
    slope_2, y_int_2 = slope_intercept(*line_2)
    # Check vertical lines
    # From next line to line before next comment, treat y_int as x_int
    if slope_1 == None and slope_2 == None:
        if y_int_1 == y_int_2:
            return True
        else:
            return False
    elif slope_1 == None:
        return y_int_1 > min(line_2[0][0], line_2[1][0]) and y_int_1 < max(line_2[0][0], line_2[1][0])
    elif slope_2 == None:
        return y_int_2 > min(line_1[0][0], line_1[1][0]) and y_int_2 < max(line_1[0][0], line_1[1][0])
    # Check equal slopes
    if slope_1 == slope_2:
        if y_int_1 == y_int_2:
            return True
        else:
            return False
    intersect_x = (y_int_2 - y_int_1) / (slope_1 - slope_2)
    intersect_y = slope_1 * intersect_x + y_int_1
    intersect_coord = (intersect_x, intersect_y)
    on_line_1 = True if round(pythagorean(intersect_coord, line_1[0]) + pythagorean(intersect_coord, line_1[1]), 1) == round(pythagorean(*line_1), 1) else False
    on_line_2 = True if round(pythagorean(intersect_coord, line_2[0]) + pythagorean(intersect_coord, line_2[1]), 1) == round(pythagorean(*line_2), 1) else False
    if on_line_1 and on_line_2:
        return True
    return False

def pythagorean(pt_1, pt_2):
    return sqrt((pt_1[0] - pt_2[0])**2 + (pt_1[1] - pt_2[1])**2)

def slope_intercept(pt_1, pt_2):
    m = get_slope(pt_1, pt_2)
    if m == None:
        return None, pt_1[0]
    b = pt_1[1] - m * pt_1[0]
    return m, b

def get_slope(pt_1, pt_2):
    if pt_1[0] - pt_2[0] == 0:
        return None
    return float((pt_1[1] - pt_2[1])) / float((pt_1[0] - pt_2[0]))

def get_valid_rand_triangle():
    x1, y1, x2, y2, x3, y3 = get_rand_triangle()
    while shoelace(x1, y1, x2, y2, x3, y3) < 1400:
        x1, y1, x2, y2, x3, y3 = get_rand_triangle()
    return x1, y1, x2, y2, x3, y3

def get_rand_triangle():
    x1 = randint(200, 500)
    y1 = randint(200, 500)
    x2 = x1 + randint(-200, 200)
    y2 = y1 + randint(-200, 200)
    x3 = x2 + randint(-200, 200)
    y3 = y2 + randint(-200, 200)
    return x1, y1, x2, y2, x3, y3

def shoelace(x1, y1, x2, y2, x3, y3):
    return 0.5 * abs(x1 * y2 + x2 * y3 + x3 * y1 - x2 * y1 - x3 * y2 - x1 * y3)
